

const { response } = 'express';

const meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto', 'Septiembre','Octubre', 'Noviembre','Diciembre'];
const  dias = ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábad'];
const date = new Date();
let dia = date.getDay()
let mes = date.getMonth()
let diaNumber = date.getDate()
let anio = date.getFullYear()

//hora
let hour = date.getHours();
let minutes = date.getMinutes()
let seconds = date.getSeconds()


const fechaGet = (req, res = response) => {
  
  res.json({
     
      day  : ` ${ dias[dia] }`,
      date_1 : `${ diaNumber } De ${meses[mes]}`,
      year   : date.getFullYear(),
      date_2 : `${ diaNumber }/${ mes + 1 }/${ anio }`,
      hour   : `${ hour }:${ minutes }:${ seconds }`,
      prefix : `${ hour > 11? 'pm': 'am' }`
      
  })

};

const personaPut = (req, res = response) => {

  res.json({
    msg: 'put API - Controlador'
  });
};

const personaPost = (req, res = response) => {

  res.json({
    msg: 'post API - Controlador',
    listado
  });
};

const personaDelete = (req, res = response) => {
  


  res.json({
    msg: 'delete API - Controlador'
  });
};

module.exports = {
  fechaGet,
  personaPut,
  personaPost,
  personaDelete
}