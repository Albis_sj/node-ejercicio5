const { Router } = require('express');
const {fechaGet } = require('../controllers/personaController');

const router = Router();

router.get('/', fechaGet);

module.exports = router;